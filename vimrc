set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" solarized
syntax enable
set background=dark
colorscheme solarized

" make backspace work
set backspace=2
" line numbers
set number

set tabstop=2
set softtabstop=2
set shiftwidth=2
set expandtab

set linebreak

set showcmd
set cursorline
set wildmenu
set lazyredraw
set showmatch

" screw them swapfile
set noswapfile

filetype indent on

set incsearch
set hlsearch

autocmd Filetype ruby setlocal tabstop=2 shiftwidth=2 expandtab commentstring=#\ %s
autocmd Filetype javascript setlocal tabstop=4 shiftwidth=4 expandtab commentstring=//\ %s

if has("autocmd")
        au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
                                \| exe "normal! g'\"" | endif
endif

" All of your Plugins must be added before the following line
Plugin 'pangloss/vim-javascript'
Plugin 'fatih/vim-go'
Plugin 'godlygeek/tabular'
Plugin 'plasticboy/vim-markdown'
Plugin 'tpope/vim-surround'

call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this lin;e
